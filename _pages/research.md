---
title: Research Areas
permalink: /research/
---

Our research group has been studying the following topics:

- Medical image fusion and corretion
- Image register with inter and intra modalities
- Physical models for imaging segmentation
- Chaos and multifractality characterization in biomedical signals
- Non linear dynamics in biological signals
- Image and signal analysis for aided diagnosis

