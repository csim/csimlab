---
title: Publications
permalink: /publications/
---
# Journal Articles 

Da S Senra Filho, A.C., Salmon, C.E.G. & Junior, L.O.M., 2015. Anomalous diffusion process applied to magnetic resonance image enhancement. Physics in Medicine and Biology, 60(6), p.2355. Available at: http://stacks.iop.org/0031-9155/60/i=6/a=2355.

Senra Filho, A.C. da S. et al., 2014. A computational tool as support in B-mode ultrasound diagnostic quality control. Revista Brasileira de Engenharia Biomédica, 30, pp.402–405. Available at: http://www.scielo.br/scielo.php?script=sci_arttext&pid=S1517-31512014000400010&lng=en&nrm=iso&tlng=en.


# Proceedings

Barizon, G.C. et al., 2014. Tissue characterization from myocardial perfusion and autonomic innervation using MRI and SPECT images in Chagas disease. In Annual Meeting of Computing in Cardiology.

Filho, A.C. da S.S., Barizon, G.C. & Junior, L.O.M., 2014. Myocardium Segmentation Improvement with Anisotropic Anomalous Diffusion Filter Applied to Cardiac Magnetic Resonance Imaging. In Annual Meeting of Computing in Cardiology.

Filho, A.C. da S.S. et al., 2014. Anisotropic Anomalous Diffusion Filtering Applied to Relaxation Time Estimation in Magnetic Resonance Imaging. In Annual International Conference of the IEEE Engineering in Medicine and Biology Society. IEEE-INST ELECTRICAL ELECTRONICS ENGINEERS INC.

Filho, A.C. da S.S. et al., 2014. Brain Activation Inhomogeneity Highlighted by the Isotropic Anomalous Diffusion Filter. In Annual International Conference of the IEEE Engineering in Medicine and Biology Society. Chicago: IEEE-INST ELECTRICAL ELECTRONICS ENGINEERS INC.

Silva, L.E.V. da et al., 2014. Two-dimensional sample entropy analysis of rat sural nerve aging. In Annual International Conference of the IEEE Engineering in Medicine and Biology Society.

Filho, A.C. da S.S., Junior, L.O.M. & Santos, A.C. dos, 2014. Anisotropic Anomalous Filter Applied to Multimodal Magnetic Resonance Image in Multiple Sclerosis. In I Transatlantic Workshop on Methods for Multimodal Neurosciences Studies. São Pedro, SP.

Senra Filho, A.C. da S., Duque, J.J. & Murta, L.O., 2013. Isotropic anomalous filtering in Diffusion-Weighted Magnetic Resonance Imaging. In I. E. in M. and B. Society, ed. 2013 35th Annual International Conference of the IEEE Engineering in Medicine and Biology Society (EMBC). Osaka, Japan: IEEE, pp. 4022–4025. Available at: http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=6610427 [Accessed October 1, 2013].

Senra Filho, A.C. da S. & Murta Junior, L.O., 2013. O USO DE FILTROS ESPACIAIS BASEADOS EM DIFUSÃO ANÔMALA PARA A DIMINUIÇÃO DO TEMPO DE AQUISIÇÃO EM MRI-DTI. In ABFM, ed. XVIII CONGRESSO BRASILEIRO DE FÍSICA MÉDICA. São Pedro, SP: ABFM.

Barbosa;, J.H.O., Senra Filho, A.C. da S.. & Salmon, C.E.G., 2012. Mapas de R2, R2* e susceptibilidade magnética de depósitos de ferro nos gânglios da base. In SBEB, ed. XXIII Congresso Brasileiro em Engenharia Biomédica. Porto de Galinhas, PE: SBEB.

Senra Filho, A.C. da S., Elias Junior, J.. & Carneiro, A.A.O., 2012. FERRAMENTA PARA AUXÍLIO NO CONTROLE DE QUALIDADE DE ULTRASSOM DIAGNÓSTICO POR IMAGEM. In SBEB, ed. XXIII Congresso Brasileiro em Engenharia Biomédica. Porto de Galinhas, PE.

Senra Filho, A.C. da S.. & Murta Junior, L.O., 2011. Equações de Difusão em Processamento de Imagens Digitais Aplicadas à Medicina. In XXIV SIBGRAPI Conference on Graphics, Patterns and Images. Maceió, AL: Sociedade Brasileira de Computação.

Senra Filho, A.C. da S. & Murta Junior, L.O., 2012. FILTRO ESPACIAL BASEADO EM EQUAÇÃO DE DIFUSÃO ISOTRÓPICA ANÔMALA. In SBEB, ed. XXIII Congresso Brasileiro em Engenharia Biomédica. Porto de Galinhas, PE.

