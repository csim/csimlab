---
layout: home
author_profile: false
---


![DCM building image](assets/images/dcm.png)

We are a computational laboratory dedicated with image and signal processing and analysis. Our research group is held in the Ribeirão Preto city, São Paulo state, Brazil and works directly with several applications related to image and signal issues. Registration, tissue segmentation, image filtering, image and signal analysis and feature extraction are some examples of 
our range studies. 

Our main goal is to improve the image and signal state-of-art and create suitable tools for many medical and biological applications.
 
