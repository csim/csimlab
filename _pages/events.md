---
Title: Scientific Events Participations
permalink: /events
---

# International Events

- [Computing in Cardiology](http://www.cinc.org/conferences.shtml) - 7-10 September 2014 Cambridge, MA, USA
- [36th Annual International Conference of the IEEE Engineering in Medicine and Biology Society](http://embc.embs.org/2014/) - August 26-30, 2014   Chicago, Illinois, USA
- [35th Annual International Conference of the IEEE Engineering in Medicine and Biology Society](http://embc.embs.org/2014/) - August 26-30, 2013   Osaka, Japan

# National Events

- [XXIV Congresso Brasileiro de Engenharia Biomédica](http://cbeb.org.br/CBEB2014/index.php/en/) - 2014 - Uberlândia, MG
- XVIII Congresso Brasileiro de Física Médica - 2013 - São Pedro, SP
- Congresso Brasileiro de Engenhraia Biomédica - 2012 - Porto de Galinhas, PE
- 24th SIBGRAPI Conference on Graphics, Patterns and Images Tutorials - 2011 - Maceió, AL
- Mais: [Wiki Call for Papers](http://www.wikicfp.com/cfp/)

