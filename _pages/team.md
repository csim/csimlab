---
title: Team
permalink: /team
---

Head:

    Prof. Dr. Luiz Otávio Murta Junior
    e-mail: murta@usp.br
      
PhD:

    Fabrício Henrique Simozo
    e-mail: fsimozo@gmail.com
    
    Juliano Jinzenji Duque
    e-mail: julianojd@gmail.com
       

    Antonio Carlos da Silva Senra Filho 
    e-mail: acsenrafilho@gmail.com
          

    Gustavo Canavaci Barizon
    e-mail: gustavo.canavacibarizon@gmail.com 
         

    Mehran Azimbagirad
    e-mail: mehrancd@gmail.com 
    web:    www.radma.ir

Master of Science:
    
    Alexandre Freitas Duarte

Ex-alunos:

    Anderson Ivan Rincón
    Mestrado (2016)
    e-mail: ander.irs@gmail.com

    Isaias José Amaral Soares
    Doutorado (2013)
    e-mail: isoares01@yahoo.com
    
    Luiz Eduardo Virgilio da Silva 
    Doutorado (2013)
    e-mail: luizeduardovs@gmail.com
    

    Mauro Guilherme Guzo
    Iniciação Científica (2011)
    e-mail: mguzo@hotmail.com
    

    Mona Miho Shinba
    Iniciação Científica (2011)
    e-mail: mona_m_s@hotmail.com
    

    Erbe Pandini Rodrigues
    Doutorado (2010)
    e-mail: erbe_p@hotmail.com
    
    
     Emylin Sousa
     Iniciação Científica (2013)

Colaboradores:
    
    Cesar Augusto Cardoso Caetano
    
