---
Title: Location
permalink: /location
---

# Head
    Prof. Luiz Otávio Murta Junior
    email: murta@usp.br
    phone: (16) 3315 3867

# Laboratory
    phone: +55 (16) 3315 0376

# University Location
    Sala IBM 524 - Bloco B1
    Department of Computing and Mathematics
    Faculty of Philosophy, Science and Letters of Ribeirão Preto
    University of São Paulo

# Address
    Av. Bandeirantes, 3900
    Bairro Monte Alegre
    Ribeirão Preto - SP
    CEP: 14040-901

