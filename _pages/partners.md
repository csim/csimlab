---
Title: Partners
permalink: /partners
---

[![Logotipo HCRP](assets/images/hc.png)](http://www.fmrp.usp.br/rcm/)
Departamento de Clínica Médica
Hospital das Clínicas - FMRP

[![Logotipo LEB](assets/images/biolab2.png)](http://www.biolab.eletrica.ufu.br/)
Laboratório de Engenharia Biomédica
Universidade Federal de Uberlândia


[![Logotipo Chieti](assets/images/chieti2.png)](http://bindcenter.eu/)
Behavioral Imaging and Neural 
Dynamics Center
Università Degli Studi G.D'Annunzio Chieti Pescara
